import 'package:animate_do/animate_do.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:pryecto_dca/core/model/pokemon.dart';
import 'package:pryecto_dca/core/providers/providers.dart';
import 'package:pryecto_dca/core/utils/utils.dart';

class ListPokemons extends ConsumerStatefulWidget {
  const ListPokemons({
    super.key,
    required this.pokemons,
  });
  final List<Pokemon>? pokemons;

  @override
  ConsumerState<ConsumerStatefulWidget> createState() => _ListPokemonsState();
}

class _ListPokemonsState extends ConsumerState<ListPokemons> {
  @override
  Widget build(BuildContext context) {
    return ListView.separated(
        itemBuilder: (context, index) => SlideInLeft(
              child: ListTile(
                title: Text(
                  widget.pokemons?[index].name ?? '',
                  style: TextStyle(
                      fontWeight: FontWeight.bold,
                      color: Colors.red,
                      fontSize: 20.sp),
                ),
                subtitle: Text(
                  widget.pokemons?[index].xdescription ?? '',
                  overflow: TextOverflow.ellipsis,
                  maxLines: 2,
                  style: TextStyle(color: Colors.grey),
                ),
                leading: CircleAvatar(
                  radius: 30,
                  backgroundColor: buildCardColor(
                      widget.pokemons?[index].typeofpokemon?.first ?? ''),
                  backgroundImage:
                      NetworkImage(widget.pokemons?[index].imageurl ?? ''),
                ),
                onTap: () {
                  ref
                      .read(pokemonProvider.notifier)
                      .getPokemonDetails(widget.pokemons?[index].id);
                  Navigator.pushNamed(context, '/details');
                },
              ),
            ),
        separatorBuilder: (context, index) => const Divider(),
        itemCount: widget.pokemons?.length ?? 0);
  }
}
