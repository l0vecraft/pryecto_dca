import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:pryecto_dca/core/providers/providers.dart';
import 'package:pryecto_dca/ui/screen/json_page.dart';
import 'package:pryecto_dca/ui/screen/pakemon_page.dart';
import 'package:pryecto_dca/ui/screen/settings_page.dart';

class HomePage extends ConsumerStatefulWidget {
  const HomePage({super.key});

  @override
  ConsumerState<ConsumerStatefulWidget> createState() => _HomePageState();
}

class _HomePageState extends ConsumerState<HomePage> {
  var _currentPage = 0;
  var _pages = [PokemonPage(), JsonPage(), SettingsPage()];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text(
          'Pokedex',
          style: TextStyle(color: Colors.white),
        ),
        centerTitle: true,
        backgroundColor: Colors.red,
      ),
      body: _pages.elementAt(_currentPage),
      bottomNavigationBar: NavigationBar(
        selectedIndex: _currentPage,
        onDestinationSelected: (value) {
          setState(() {
            _currentPage = value;
          });
          if (value == 1) {
            ref.read(jsonProvider.notifier).loadPokemon();
          }
        },
        indicatorColor: Colors.red,
        elevation: 10,
        shadowColor: Colors.black,
        destinations: const [
          NavigationDestination(
              icon: Icon(Icons.list_alt_rounded), label: 'pokemon'),
          NavigationDestination(
              icon: Icon(Icons.add_box_outlined), label: 'json'),
          NavigationDestination(icon: Icon(Icons.settings), label: 'settings'),
        ],
      ),
    );
  }
}
