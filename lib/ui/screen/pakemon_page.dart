import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:pryecto_dca/core/providers/providers.dart';
import 'package:pryecto_dca/ui/widget/custom_progress_indicator.dart';
import 'package:pryecto_dca/ui/widget/list_pokemons.dart';

class PokemonPage extends ConsumerStatefulWidget {
  const PokemonPage({super.key});

  @override
  ConsumerState<ConsumerStatefulWidget> createState() => _PokemonPageState();
}

class _PokemonPageState extends ConsumerState<PokemonPage> {
  @override
  Widget build(BuildContext context) {
    var pokeData = ref.watch(pokemonFutureProvider);

    return Container(
      child: pokeData.when(
        data: (data) => ListPokemons(pokemons: data),
        error: (error, stackTrace) => Center(
          child: Text("$error"),
        ),
        loading: () => const CustomProgressIndicator(),
      ),
    );
  }
}
