import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:pryecto_dca/core/providers/providers.dart';
import 'package:pryecto_dca/ui/widget/custom_progress_indicator.dart';

class DetailsPage extends ConsumerWidget {
  const DetailsPage({super.key});

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final state = ref.watch(pokemonProvider);
    return (state.isLoading ?? true)
        ? Container(color: Colors.white, child: const CustomProgressIndicator())
        : Scaffold(
            appBar: AppBar(
              leading: IconButton(
                  onPressed: () => Navigator.pop(context),
                  icon: const Icon(
                    Icons.arrow_back_ios_rounded,
                    color: Colors.white,
                  )),
              title: Text(
                state.pokemonSelected?.name ?? "Details",
                style: const TextStyle(color: Colors.white),
              ),
              centerTitle: true,
              backgroundColor: Colors.green,
            ),
            body: const Center(child: Text('Details')),
          );
  }
}
