import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

class SettingsPage extends ConsumerStatefulWidget {
  const SettingsPage({super.key});

  @override
  ConsumerState<ConsumerStatefulWidget> createState() => _SettingsPageState();
}

class _SettingsPageState extends ConsumerState<SettingsPage> {
  var _controller = TextEditingController();
  var _formKey = GlobalKey<FormState>();
  var _texto = '';
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Form(
        key: _formKey,
        child: Column(
          children: [
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: TextFormField(
                controller: _controller,
                validator: (value) =>
                    (value?.isEmpty ?? false) ? "Rellena el campo" : null,
                onSaved: (newValue) {
                  setState(() {
                    _texto = newValue ?? '';
                  });
                  print(_texto);
                },
              ),
            ),
            Text(
              _texto,
              style: TextStyle(fontSize: 30),
            ),
            RawMaterialButton(
              onPressed: () {
                if ((_formKey.currentState?.validate() ?? false)) {
                  _formKey.currentState?.save();
                }
              },
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Text('Guardar datos'),
              ),
              fillColor: Colors.amber,
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(20)),
            )
          ],
        ),
      ),
    );
  }
}
