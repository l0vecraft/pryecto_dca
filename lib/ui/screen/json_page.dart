import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:pryecto_dca/core/providers/providers.dart';
import 'package:pryecto_dca/ui/widget/custom_progress_indicator.dart';

class JsonPage extends ConsumerStatefulWidget {
  const JsonPage({super.key});

  @override
  ConsumerState<ConsumerStatefulWidget> createState() => _JsonPageState();
}

class _JsonPageState extends ConsumerState<JsonPage> {
  @override
  Widget build(BuildContext context) {
    var state = ref.watch(jsonProvider);
    var viewController = ref.read(jsonProvider.notifier);
    return Container(
      child: (state.isLoading ?? false)
          ? const CustomProgressIndicator()
          : Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(state.data?.name != null
                      ? (state.data?.name ?? '')
                      : 'No Hay datos aun'),
                  if (state.data?.id?.isNotEmpty ?? false)
                    RawMaterialButton(
                      onPressed: () => viewController.deletePokemon(),
                      fillColor: Colors.red,
                      child: const Padding(
                        padding: EdgeInsets.all(10.0),
                        child: Text(
                          "Borrar datos",
                          style: TextStyle(
                              color: Colors.white, fontWeight: FontWeight.bold),
                        ),
                      ),
                    )
                ],
              ),
            ),
    );
  }
}
