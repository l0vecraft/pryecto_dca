part of 'providers.dart';

class JsonController extends StateNotifier<JsonState> {
  JsonController() : super(JsonState());

  final _pokeStored = Hive.box<Pokemon?>('pokemon');

  Future<void> loadPokemon() async {
    try {
      state = state.copyWith(isLoading: true);
      final result = _pokeStored.getAt(0);
      if (result != null) {
        state = state.copyWith(data: result);
      }
      state = state.copyWith(isLoading: false);
      _pokeStored.values.forEach((element) {
        print(
            "name: ${element?.name} descrip: ${element?.xdescription} peso: ${element?.weight}");
      });
    } catch (e) {
      state = state.copyWith(data: Pokemon(), isLoading: false);
      print("Error al cargar el pokemon => $e");
    }
  }

  Future<void> deletePokemon() async {
    try {
      state = state.copyWith(isLoading: true);

      await _pokeStored.clear();
      state = state.copyWith(data: Pokemon(), isLoading: false);
    } catch (e) {
      print("Error al borrar el pokemon => $e");
    }
  }
}
