library pryecto_dca.core.providers;

import 'package:dio/dio.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:hive/hive.dart';
import 'package:pryecto_dca/core/data/network/poke_api.dart';
import 'package:pryecto_dca/core/data/repository/poke_repository.dart';
import 'package:pryecto_dca/core/model/pokemon.dart';
import 'package:pryecto_dca/core/model/pokemon_details.dart';

part 'pokemon_controller.dart';
part 'pokemon_state.dart';
part 'json_controller.dart';
part 'json_state.dart';

var _dio = Dio();

var _api = PokeApi(dio: _dio);

final pokemonProvider = StateNotifierProvider<PokemonController, PokemonState>(
    (ref) => PokemonController(api: _api));

final pokemonFutureProvider = FutureProvider<List<Pokemon>>(
    (ref) async => await ref.read(pokemonProvider.notifier).getPokemon());

final jsonProvider =
    StateNotifierProvider<JsonController, JsonState>((ref) => JsonController());
