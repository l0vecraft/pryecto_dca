part of 'providers.dart';

class PokemonState {
  PokemonState({
    required this.pokemones,
    this.isLoading = false,
    this.pokemonDetail,
    this.pokemonSelected,
  });

  final List<Pokemon> pokemones;

  final bool? isLoading;
  final Pokemon? pokemonSelected;
  final PokemonDetail? pokemonDetail;

  int get edad => 28;

  PokemonState copyWith(
      {List<Pokemon>? pokemones,
      bool? isLoading,
      Pokemon? pokemonSelected,
      PokemonDetail? pokemonDetail}) {
    return PokemonState(
      pokemones: pokemones ?? this.pokemones,
      isLoading: isLoading ?? this.isLoading,
      pokemonDetail: pokemonDetail ?? this.pokemonDetail,
      pokemonSelected: pokemonSelected ?? this.pokemonSelected,
    );
  }
}
