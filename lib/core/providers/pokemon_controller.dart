part of 'providers.dart';

class PokemonController extends StateNotifier<PokemonState> {
  PokemonController({required this.api})
      : super(PokemonState(pokemones: <Pokemon>[]));
  final PokeRepository api;

  Future<List<Pokemon>> getPokemon() async {
    var result = <Pokemon>[];
    try {
      result = await api.getPokemonData();
      state = state.copyWith(pokemones: result);
    } catch (e) {
      print("Upss  error => $e");
    }
    return result;
  }

  Future<void> getPokemonDetails(String? id) async {
    var parsedId = int.parse(id?.replaceAll("#", "") ?? "");
    try {
      state = state.copyWith(isLoading: true);
      var selectedPokemon =
          state.pokemones.where((element) => element.id == id).first;
      var result = await api.getDetailPokemon(parsedId);
      state = state.copyWith(
          pokemonDetail: result,
          pokemonSelected: selectedPokemon,
          isLoading: false);
      _savePokemonSelected(state.pokemonSelected);
    } catch (e) {
      print("Upps=> $e");
      rethrow;
    }
  }

  Future<void> _savePokemonSelected(Pokemon? pokemon) async {
    try {
      var pokeToSave = Hive.box<Pokemon?>('pokemon');
      await pokeToSave.put(pokemon?.id, pokemon);
    } catch (e) {
      print("Error al intentar guardar el pokemon=> $e");
    }
  }

  Future<void> _deletePokemonSelected(String? id) async {
    try {
      var pokeToDelete = Hive.box<Pokemon?>('pokemon');
      await pokeToDelete.delete(id);
    } catch (e) {
      print("Error al intentar borrar el pokemon=> $e");
    }
  }
}
