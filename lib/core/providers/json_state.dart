part of 'providers.dart';

class JsonState {
  JsonState({this.data, this.isLoading = false});

  final Pokemon? data;
  final bool? isLoading;

  JsonState copyWith({
    Pokemon? data,
    bool? isLoading,
  }) =>
      JsonState(
          data: data ?? this.data, isLoading: isLoading ?? this.isLoading);
}
