import 'package:dio/dio.dart';
import 'package:pryecto_dca/core/data/repository/poke_repository.dart';
import 'package:pryecto_dca/core/model/pokemon.dart';
import 'package:pryecto_dca/core/model/pokemon_details.dart';
import 'package:pryecto_dca/core/utils/utils.dart';

class PokeApi implements PokeRepository {
  PokeApi({required this.dio});

  final Dio dio;

  @override
  Future<List<Pokemon>> getPokemonData() async {
    var result = <Pokemon>[];

    try {
      var response = await readJson('assets/pokemon.json');
      result = response.map((pokemon) => Pokemon.fromJson(pokemon)).toList();
    } catch (e) {
      print("Error=> $e");
    }
    return result;
  }

  @override
  Future<PokemonDetail> getDetailPokemon(int? id) async {
    try {
      final response = await dio.get('https://pokeapi.co/api/v2/pokemon/$id');
      return PokemonDetail.fromJson(response.data);
    } catch (e) {
      rethrow;
    }
  }
}
