import 'package:pryecto_dca/core/model/pokemon.dart';
import 'package:pryecto_dca/core/model/pokemon_details.dart';

abstract class PokeRepository {
  Future<List<Pokemon>> getPokemonData();
  Future<PokemonDetail> getDetailPokemon(int? id);
}
