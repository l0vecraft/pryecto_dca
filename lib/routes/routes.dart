import 'package:flutter/material.dart';
import 'package:pryecto_dca/ui/screen/details_page.dart';
import 'package:pryecto_dca/ui/screen/home_page.dart';

class Routes {
  static Route onGenerateRoutes(RouteSettings settings) {
    switch (settings.name) {
      case '/':
        return MaterialPageRoute(builder: (context) => const HomePage());
      case '/details':
        // return MaterialPageRoute(builder: (context) => const DetailsPage());
        return PageRouteBuilder(
          pageBuilder: (context, animation, secondaryAnimation) {
            var begin = const Offset(1.0, 0.0);
            var end = Offset.zero;
            var tween = Tween<Offset>(begin: begin, end: end)
                .chain(CurveTween(curve: Curves.easeIn));
            return SlideTransition(
              position: animation.drive(tween),
              child: const DetailsPage(),
            );
          },
        );
      default:
        return MaterialPageRoute(builder: (context) => const HomePage());
    }
  }
}
