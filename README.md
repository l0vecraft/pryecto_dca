# pryecto_dca



## Getting started
a continuacion las depedencias iniciales que se trabajaran en el proyecto.

    animate_do: ^3.3.4
    cupertino_icons: ^1.0.6
    dio: ^5.4.2+1
    flutter_native_splash: ^2.4.0
    flutter_riverpod: ^2.5.1
    flutter_screenutil: ^5.9.0
    go_router: ^13.2.2
    hive: ^2.2.3
    hive_flutter: ^1.1.0
    path_provider: ^2.1.2

    dev_dependencies:

    
    build_runner: ^2.3.3
    flutter_lints: ^3.0.0
    hive_generator: ^2.0.0

este proyecto es realizado con fines de aprendizaje y el nivel que se considero es del mas basico. Se estara realizando un ejemplo popular de la Pokedex.

## Recursos
utilizaremos inicialmente un archivo JSON bastante completo y solo con los datos basicos que
tienen los pokemones el cual podemos encontrar en 

- carpeta `./assets/pokemon.json`

agradecimientos al usuario https://github.com/hungps pueden revisar su proyecto de pokedex en https://github.com/hungps/flutter_pokedex

tambien usaremos la poke api oficial para obtener informacion detallada de cada uno de los datos y en caso de que se quiera implementar una pantalla de detalles esta informacion nos ayudara bastante

https://pokeapi.co/